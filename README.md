# Devcontainers Templates - GCloud CLI

This project is a [Devcontainer](.devcontainer/) template built against both
`alpine` and `debian` images of the
[Basics template](https://gitlab.com/geekstuff.dev/devcontainers/templates/basics).

It adds the [gcloud-cli feature](https://gitlab.com/geekstuff.dev/devcontainers/features/gcloud-cli)
on top.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "name": "my devcontainer",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/gcloud-cli/alpine/3.17",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/gcloud-cli": {
            "project": "gs-vault-dev",
            "region": "us-central1"
        }
    }
}
```

From the example above:

- Image `alpine/3.17` can be changed to `debian/bullseye`.
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from
  [tags in this project](https://gitlab.com/geekstuff.dev/devcontainers/templates/gcloud-cli/container_registry).
- The template already includes gcloud-cli, but here we "re-call" it to simply set GCP project and region.
- See the basics template for info on how to cache VSCode extensions.
